import 'package:flutter/material.dart';

class TestComponent extends StatefulWidget {
  const TestComponent({Key? key, this.title = 'Ваш текст', this.isChecked = false}) : super(key: key);
  final String title;
  final bool isChecked;
  @override
  State<TestComponent> createState() => _TestComponent();
}

class _TestComponent extends State<TestComponent> {
  double _secondCellWidth = 140.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Flexible(
                flex: _secondCellWidth > 10 ? 0 : 1,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                  height: 50,
                  decoration: BoxDecoration(border: (Border.all())),
                  child: Center(
                    child: Text(
                      widget.title,
                      style: const TextStyle(color: Colors.black),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ),
              _secondCellWidth > 10
                  ? Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        height: 50,
                        decoration: BoxDecoration(border: (Border.all())),
                        child: LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints constraints) {
                            final secondCellWidth = constraints.maxWidth / 2;
                            if (secondCellWidth != _secondCellWidth) {
                              WidgetsBinding.instance.addPostFrameCallback((_) {
                                setState(() {
                                  _secondCellWidth = secondCellWidth;
                                });
                              });
                            }
                            return const DottedSeparatorWidget();
                          },
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
              Container(
                height: 50,
                decoration: BoxDecoration(border: (Border.all())),
                child: Center(
                  child: Checkbox(
                    value: widget.isChecked,
                    onChanged: (bool? value) {},
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DottedSeparatorWidget extends StatelessWidget {
  const DottedSeparatorWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        const dashWidth = 10.0;
        const dashHeight = 1.0;
        final dashCount = (boxWidth / (2 * dashWidth)).round();
        return Flex(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
          children: List.generate(dashCount, (_) {
            return const SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: Colors.black),
              ),
            );
          }),
        );
      },
    );
  }
}
